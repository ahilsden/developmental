function convertToRoman(num) {
  let numeralBuilder = [];
  let factor = 1;

  const romanNumeralGroups = {
    1: ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX'],
    10: ['X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC'],
    100: ['C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM'],
  }

  num
    .toString()
    .split('')
    .reverse()
    .map(number => {
      if (number !== 0) {
        let numberMultiplied = number * factor;
        let decimal = determineDecimal(factor, numberMultiplied);
        getNumeral(factor, decimal, number, numeralBuilder, romanNumeralGroups);
      }

      factor *= 10;

    });

  return numeralBuilder.reverse().join('');
}

function getNumeral(factor, decimal, number, numeralBuilder, romanNumeralGroups) {

  if (factor === 1000) {
    for (let i = 0; i < number; i++) {
      numeralBuilder.push('M');
    }

    return numeralBuilder;
  }

  switch (decimal) {
    case '1.00':
      return numeralBuilder.push(romanNumeralGroups[factor][0]);
    case '0.50':
      return numeralBuilder.push(romanNumeralGroups[factor][1]);
    case '0.33':
      return numeralBuilder.push(romanNumeralGroups[factor][2]);
    case '0.25':
      return numeralBuilder.push(romanNumeralGroups[factor][3]);
    case '0.20':
      return numeralBuilder.push(romanNumeralGroups[factor][4]);
    case '0.17':
      return numeralBuilder.push(romanNumeralGroups[factor][5]);
    case '0.14':
      return numeralBuilder.push(romanNumeralGroups[factor][6]);
    case '0.13':
      return numeralBuilder.push(romanNumeralGroups[factor][7]);
    case '0.11':
      return numeralBuilder.push(romanNumeralGroups[factor][8]);
  }
}

function determineDecimal(factor, numberMultiplied) {
  return Number.parseFloat(factor / numberMultiplied).toFixed(2);
}

const answer = convertToRoman(4309);
const projectName = 'Roman Numeral Converter';
