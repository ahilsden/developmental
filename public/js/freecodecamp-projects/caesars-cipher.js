function rot13(str) {

  const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const shift = 13;

  let cipher = [];

  str.split('').map(rotLetter => {
    if (rotLetter.match(/\s|\W/)) {
      cipher.push(rotLetter);
    }
    alphabet.split('').map((alphaLetter, index) => {
      if (rotLetter === alphaLetter) {
        let shiftedIndex = index + shift > 25
          ? index - shift
          : index + shift;
        cipher.push(alphabet.split('')[shiftedIndex]);
      }
    });
  });

  return cipher.join('');
}

rot13("SERR PBQR PNZC!");
