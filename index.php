<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta property="og:title" content="">
  <meta property="og:type" content="">
  <meta property="og:url" content="">
  <meta property="og:image" content="">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="public/css/normalize.css">
  <link rel="stylesheet" href="public/css/main.css">
  <link rel="stylesheet" href="public/css/custom.css">
  <link rel="stylesheet" href="public/css/js-projects.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
        crossorigin="anonymous">

  <meta name="theme-color" content="#fafafa">
</head>

<body>
<div class="container-fluid text-center">
  <h1>Development-al</h1>
  <img src="public/img/me-black-and-white.jpeg" height="200" width="200" alt="Alyn photo"/>
  <h2>Today's objectives...</h2>
  <h3>20 October 2020</h3>
  <ul>
    <li>Revision: Finish Roman Numeral Converter (FCC JS projects) - Done</li>
    <li>Decide on suitable IDE for full stack dev work - PhpStorm</li>
    <li>Get Mac up to date (i.e. sort Xcode and Git issue) - Xcode installed. Git working again.</li>
    <li>Create subdomain for my new website - Done</li>
    <li>Decide on Github or Bitbucket for remote repositories - Bitbucket</li>
    <li>Sort deployment solution - Bitbucket Pipeline</li>
    <li>Have very basic (and I MEAN, basic!) website deployed to developmental.alynhilsden.com - Done</li>
    <li>Take screenshot - Done</li>
  </ul>

  <h3>21 October 2020</h3>
  <ul>
    <li>Refactor Roman Numeral Converter - Done</li>
    <li>Revision: Do FCC HTML and CSS sections - Done</li>
    <li>Take screenshot - Done</li>
  </ul>

  <h3>22 October 2020</h3>
  <ul>
    <li>Revision: Caesar's Cipher (FCC JS projects) - Done</li>
    <li>Revision: Do FCC Bootstrap section - Done</li>
    <li>Work through Quincy Larson video on Docker/dev ops - In progress</li>
    <li>Set up local env for Php development (probably Docker containers) - Not done</li>
    <li>List requirements for simple (non-Ajax) to-do list component - Not done</li>
    <li>Take screenshot - done</li>
  </ul>

  <h3>23 October 2020</h3>
  <ul>
    <li>Refactor: Caesar's Cipher - Done</li>
    <li>Work through Quincy Larson video on Docker/dev ops - In progress</li>
    <li>Set up local env for Php development (probably Docker containers) - In progress</li>
    <li>Look at project management options - Done (probably Jira)</li>
    <li>Take screenshot - Done</li>
  </ul>

  <h3>24 October 2020</h3>
  <ul>
    <li>Work through Quincy Larson video on Docker/dev ops - Done</li>
    <li>Set up local env for PHP/MySQL development (probably Docker containers) - Done</li>
    <li>Sort Git and MySql GUI clients (probably SourceTree and Workbench) - Done</li>
    <li>Revision: FCC jQuery section</li>
  </ul>

  <h3>25 October 2020</h3>
  <ul>
    <li>Revision: Telephone Number Validator (FCC JS projects) - In progress</li>
  </ul>

  <h3>26 October 2020</h3>
  <ul>
    <li>Revision: Telephone Number Validator (FCC JS projects) - In progress</li>
    <li>Design simple MVC website: Blog, Todo (child of Blog), Admin, Authentication</li>
  </ul>

  <h2>Long term objectives...</h2>
  <h3>Blog post ideas:</h3>
  <ul>
    <li>Limitations of algorithms</li>
    <li>Coding and personality type</li>
    <li>Book reviews: Deep Work etc.</li>
    <li>Disruption</li>
  </ul>

  <a href="public/sandbox.php">Sandbox page</a>

  <h2>JavaScript Revision - FreeCodeCamp Projects</h2>
  <h3 id="project-title"></h3>
  <p id="answer"></p>
</div>

  <script src="public/js/vendor/modernizr-3.11.2.min.js"></script>
  <script src="public/js/plugins.js"></script>
  <script src="public/js/main.js"></script>
  <script src="public/js/freecodecamp-projects/roman-numerals.js"></script>

  <script>document.getElementById('answer').innerHTML = answer</script>
  <script>document.getElementById('project-title').innerHTML = projectName</script>

  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set', 'anonymizeIp', true); ga('set', 'transport', 'beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async></script>
</body>

</html>
