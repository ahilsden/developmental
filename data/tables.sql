-- Setting host to '%' to connect with externally (i.e. to Workbench)
-- https://github.com/docker-library/mysql/issues/275
UPDATE mysql.user
SET host = '%'
WHERE user='root';

CREATE database sandbox;
USE sandbox;
